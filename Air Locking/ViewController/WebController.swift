//
//  WebController.swift
//  Air Locking
//
//  Created by Digital Dividend on 5/25/18.
//  Copyright © 2018 Syed Shah Hasan. All rights reserved.
//

import UIKit
import WebKit

class WebController: UIViewController, WKUIDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    public var url: String = ""
    
     var refController:UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refController.bounds = CGRect(x: 0, y: 50, width: refController.bounds.size.width, height: refController.bounds.size.height)
        refController.addTarget(self, action: #selector(self.pullDownToRefresh(_:)), for: .valueChanged)
        webView.scrollView.addSubview(refController)
        
        let myURL = URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        let myRequest = URLRequest(url: myURL!)
        webView.loadRequest(myRequest)
        self.webView.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func pullDownToRefresh(_ sender: UIRefreshControl) {
        self.webView.reload()
        self.refController.endRefreshing()
    }
}

// MARK: -  UIWEBVIEW DELEGATE
extension WebController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("loading completed")
        Utility.main.hideLoader()
        self.refController.endRefreshing()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print("loading starts")
        Utility.main.showLoader()
        return true
    }
}
