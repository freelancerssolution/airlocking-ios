//
//  PinNumber.swift
//  Air Locking
//
//  Created by Digital Dividend on 5/25/18.
//  Copyright © 2018 Syed Shah Hasan. All rights reserved.
//

import UIKit
import AudioToolbox
class PinNumber: UIViewController {

    @IBOutlet weak var btnDotOne: UIButton!
    @IBOutlet weak var btnDotTwo: UIButton!
    @IBOutlet weak var btnDotThree: UIButton!
    @IBOutlet weak var btnDotFour: UIButton!
    
    @IBOutlet weak var viewPinDot: UIView!
    @IBOutlet weak var lblSetPin: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    
    var numberOfAttempt: Int = 0
    var pinArray = [Int]()
    
    var pinStr = ""
    var confirmPinStr = ""
    
    public var permenantToken: String = ""
    public var isDirectlyComingToThisController: Bool = false
    
    var isPinSet: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.isDirectlyComingToThisController {
            if let pinCode = Constants.USER_DEFAULTS.value(forKey: UserDefaultsContansts.PinCode) as? String, let permToken = Constants.USER_DEFAULTS.value(forKey: UserDefaultsContansts.PermenantToken) as? String {
                self.pinStr = pinCode
                self.permenantToken = permToken
            }
            self.isPinSet = true
            self.lblSetPin.text = Messages.enterPinNumber.rawValue
            self.btnLogin.isHidden = false
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
// MARK: - HELPER METHODS
extension PinNumber {
    func addIntToPinArray(_ pinNumber: Int) {
        if self.pinArray.count <= 3 {
            self.pinArray.append(pinNumber)
            if pinArray.count == 4 {
                if !self.isPinSet {
                    self.lblSetPin.text = Messages.confrimPinNumber.rawValue
                    self.deselectAllDots()
                    self.pinArrayToString(with: self.pinArray)
                    self.pinArray.removeAll()
                    self.isPinSet = true
                } else {
                    self.pinArrayToConfirmString(with: self.pinArray)
                    print("I am confirm pin all a service \(self.pinStr) == \(self.confirmPinStr)")
                    self.checkThePinToProceed()
                }
                print("pin number: \(self.pinArray)")
            }
        } else {
            return
        }
        
        switch self.pinArray.count {
        case 1:
            self.btnDotOne.isSelected = true
            
            self.btnDotTwo.isSelected = false
            self.btnDotThree.isSelected = false
            self.btnDotFour.isSelected = false
            break
        case 2:
            self.btnDotOne.isSelected = true
            self.btnDotTwo.isSelected = true
            
            self.btnDotThree.isSelected = false
            self.btnDotFour.isSelected = false
            break
        case 3:
            self.btnDotOne.isSelected = true
            self.btnDotTwo.isSelected = true
            self.btnDotThree.isSelected = true
            
            self.btnDotFour.isSelected = false
            break
        case 4:
            self.btnDotOne.isSelected = true
            self.btnDotTwo.isSelected = true
            self.btnDotThree.isSelected = true
            self.btnDotFour.isSelected = true
            break
        default:
            self.btnDotOne.isSelected = false
            self.btnDotTwo.isSelected = false
            self.btnDotThree.isSelected = false
            self.btnDotFour.isSelected = false
        }
    }
    
    func resetCompleteController() {
        self.deselectAllDots()
        self.pinStr = ""
        self.confirmPinStr = ""
        self.pinArray.removeAll()
        self.isPinSet = false
        self.lblSetPin.text = Messages.setPinNumber.rawValue
    }
    
    func deselectAllDots() {
        self.btnDotOne.isSelected = false
        self.btnDotTwo.isSelected = false
        self.btnDotThree.isSelected = false
        self.btnDotFour.isSelected = false
    }
    
    func pinArrayToString(with pinArrayToStr: [Int]) {
        self.pinStr = ""
        for pinInt in pinArrayToStr {
            self.pinStr += "\(pinInt)"
        }
    }
    
    func pinArrayToConfirmString(with pinArrayToStr: [Int]) {
        self.confirmPinStr = ""
        for pinInt in pinArrayToStr {
            self.confirmPinStr += "\(pinInt)"
        }
    }
    
    func checkThePinToProceed() {
        self.numberOfAttempt += 1
        if self.pinStr == self.confirmPinStr {
            print("proceed")
            self.getTheTempToken()
        } else {
            print("print miss match")
            self.viewPinDot.shake()
            self.deselectAllDots()
            self.pinArray.removeAll()
            self.isPinSet = true
            
            if self.numberOfAttempt >= 5 {
                self.removeAllCacheData()
            }
        }
    }
    
    // it will remove all locally store data and move controller to login
    func removeAllCacheData() {
        Constants.USER_DEFAULTS.removeObject(forKey: UserDefaultsContansts.PermenantToken)
        Constants.USER_DEFAULTS.removeObject(forKey: UserDefaultsContansts.PinCode)
        
        Constants.APP_DELEGATE.changeRootToLogin()
    }
}
// MARK: - IB ACTIONS
extension PinNumber {
    @IBAction func actionOne(_ sender: Any) {
        self.addIntToPinArray(1)
    }
    @IBAction func actionTwo(_ sender: Any) {
        self.addIntToPinArray(2)
    }
    @IBAction func actionThree(_ sender: Any) {
        self.addIntToPinArray(3)
    }
    @IBAction func actionFour(_ sender: Any) {
        self.addIntToPinArray(4)
    }
    @IBAction func actionFive(_ sender: Any) {
        self.addIntToPinArray(5)
    }
    @IBAction func actionSix(_ sender: Any) {
        self.addIntToPinArray(6)
    }
    @IBAction func actionSeven(_ sender: Any) {
        self.addIntToPinArray(7)
    }
    @IBAction func actionEight(_ sender: Any) {
        self.addIntToPinArray(8)
    }
    @IBAction func actionNine(_ sender: Any) {
        self.addIntToPinArray(9)
    }
    @IBAction func actionZero(_ sender: Any) {
        self.addIntToPinArray(0)
    }
    @IBAction func actionLogin(_ sender: Any) {
        if let login = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "Login") as? Login {
            login.comingFromPinController = true
            self.navigationController?.pushViewController(login, animated: true)
        }
    }
}

// MARK: - API CALLING
extension PinNumber {
    func getTheTempToken() {
        if !self.permenantToken.isEmpty {
            let parameter = [
                "permtoken" : self.permenantToken
            ]
            Utility.main.showLoader()
            APIManager.sharedInstance.usersAPIManager.GetTempToken(params: parameter, success: { (succes) in
                if let tempToken = succes["temptoken"] as? String {
                    Constants.USER_DEFAULTS.set(self.pinStr, forKey: UserDefaultsContansts.PinCode)
                    Constants.USER_DEFAULTS.set(self.permenantToken, forKey: UserDefaultsContansts.PermenantToken)
                    let webVC = SwiftWebVC(urlString: "\(Constants.WEBBASEURL)\(tempToken)")
                    webVC.sharingEnabled = false
                    webVC.delegate = self
                    self.navigationController?.pushViewController(webVC, animated: true)
                    
//                    if let webController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "WebController") as? WebController {
//                        webController.url = "\(Constants.WEBBASEURL)\(tempToken)"//Constants.MobileRegistrationURL
//                        self.navigationController?.navigationBar.isHidden = true
//                        self.navigationController?.pushViewController(webController, animated: true)
//                    }
                }
                Utility.main.hideLoader()
            }) { (failure) in
                Utility.main.hideLoader()
                self.resetCompleteController()
            }
        } else {
            Utility.main.showAlert(message: Messages.invalidToken.rawValue, title: Messages.alertStr.rawValue)
        }
    }
}

extension PinNumber: SwiftWebVCDelegate {
    func didStartLoading() {
        Utility.main.showLoader()
    }
    func didFinishLoading(success: Bool) {
        Utility.main.hideLoader()
    }
}
