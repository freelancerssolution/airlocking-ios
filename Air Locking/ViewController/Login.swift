//
//  Login.swift
//  Air Locking
//
//  Created by Digital Dividend on 5/25/18.
//  Copyright © 2018 Syed Shah Hasan. All rights reserved.
//

import UIKit

class Login: UIViewController {

    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    public var comingFromPinController: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.textFieldEmail.text = "complex@airlocking.com"
//        self.textFieldPassword.text = "lock1Enter."
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        let parameter: [String : Any] = [
            "username" : self.textFieldEmail.text!,
            "password" : self.textFieldPassword.text!
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.GetPermenantToken(params: parameter, success: { (succss) in
            Utility.main.hideLoader()
            if let permToken = succss["permtoken"] as? String {
                if self.comingFromPinController {
                    self.getTheTempToken(with: permToken)
                } else {
                    if let pinCode = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "PinNumber") as? PinNumber {
                        pinCode.permenantToken = permToken
                        self.navigationController?.pushViewController(pinCode, animated: true)
                    }
                }

            }
            else {
                Utility.main.showAlert(message: Messages.tokenFetchingFail.rawValue, title: Messages.alertStr.rawValue)
            }
        }) { (failure) in
            Utility.main.hideLoader()
        }
    }
    
    func getTheTempToken(with permenantToken: String) {
        if !permenantToken.isEmpty {
            let parameter = [
                "permtoken" : permenantToken
            ]
            Utility.main.showLoader()
            APIManager.sharedInstance.usersAPIManager.GetTempToken(params: parameter, success: { (succes) in
                if let tempToken = succes["temptoken"] as? String {
                    Constants.USER_DEFAULTS.set(permenantToken, forKey: UserDefaultsContansts.PermenantToken)
                    let webVC = SwiftWebVC(urlString: "\(Constants.WEBBASEURL)\(tempToken)")
                    webVC.sharingEnabled = false
                    webVC.delegate = self
                    self.navigationController?.pushViewController(webVC, animated: true)
                }
                Utility.main.hideLoader()
            }) { (failure) in
                Utility.main.hideLoader()
            }
        } else {
            Utility.main.showAlert(message: Messages.invalidToken.rawValue, title: Messages.alertStr.rawValue)
        }
    }
    
    @IBAction func actionForgotPassword(_ sender: Any) {
        if let webController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "WebController") as? WebController {
            webController.url = Constants.ForgotPasswordWebURL
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(webController, animated: true)
        }
    }
    @IBAction func actionRegister(_ sender: Any) {
        if let webController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "WebController") as? WebController {
            webController.url = Constants.MobileRegistrationURL
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(webController, animated: true)
        }
    }
    
    @IBAction func actionDidChangePassword(_ sender: Any) {
        if let textFiled = sender as? UITextField {
            if Validation.isValidePassword(value: textFiled.text!) && Validation.isValidEmail(self.textFieldEmail.text!) {
                self.btnLogin.isEnabled = true
                self.btnLogin.alpha = 1
            } else {
                self.btnLogin.isEnabled = false
                self.btnLogin.alpha = 0.3
            }
        }
    }
    @IBAction func actionDidChangeEmail(_ sender: Any) {
        if let textFiled = sender as? UITextField {
            if Validation.isValidEmail(textFiled.text!) && Validation.isValidePassword(value: self.textFieldPassword.text!) {
                self.btnLogin.isEnabled = true
                self.btnLogin.alpha = 1
            } else {
                self.btnLogin.isEnabled = false
                self.btnLogin.alpha = 0.3
            }
        }
        
    }
}


extension Login: SwiftWebVCDelegate {
    func didStartLoading() {
        Utility.main.showLoader()
    }
    
    func didFinishLoading(success: Bool) {
        Utility.main.hideLoader()
    }
}
