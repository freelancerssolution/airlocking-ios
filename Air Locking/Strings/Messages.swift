//
//  Messeges.swift
//  Air Locking
//
//  Created by Digital Dividend on 5/22/18.
//  Copyright © 2018 Air Locking. All rights reserved.
//

import Foundation


enum Messages: String {
    // MARK: API MANAGER
    case errorInternetConnection  = "Your Internet connection is not working properly or server is not responding, please retry."
    case errorInResponce           = "Server is not responding correctly, please retry."
    case alertStr = "Alert"
    case invalidToken = "Invalid Token"
    case tokenFetchingFail = "Unable to fetch token"
    case confrimPinNumber = "Confirm Pin Number"
    case setPinNumber = "Set Pin Number"
    case enterPinNumber = "Enter Your Pin Number"
}
