//
//  APIManager.swift
//  Air Locking
//
//  Created by Digital Dividend on 5/21/18.
//  Copyright © 2018 Air Locking. All rights reserved.
//

import Foundation
import UIKit

typealias DefaultAPIFailureClosure = (NSError) -> Void
typealias DefaultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void
typealias DefaultBoolResultAPISuccesClosure = (Bool) -> Void
typealias DefaultArrayResultAPISuccessClosure = (Array<AnyObject>) -> Void
typealias DefaultImageResultClosure = (UIImage) -> Void
// download closures
typealias DefaultDownloadSuccessClosure = (Data) -> Void
typealias DefaultDownloadProgressClosure = (Double, Int) -> Void
typealias DefaultDownloadFailureClosure = (NSError, Data, Bool) -> Void


protocol APIErrorHandler {
    func handleErrorFromResponse(response: Dictionary<String,AnyObject>)
    func handleErrorFromERror(error:NSError)
}


class APIManager: NSObject {
    
    
    static let sharedInstance = APIManager()
    
    var serverToken: String? {
        get{
            return ""
        }
    }
    
    var accessToken: String? {
        get {
            return ""
        }
    }
    
    let usersAPIManager = UsersAPIManager()
    
}
