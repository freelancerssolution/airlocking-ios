//
//  UserAPIManager.swift
//  Air Locking
//
//  Created by Digital Dividend on 5/21/18.
//  Copyright © 2018 Air Locking. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    
    //MARK: - /Login
    func GetPermenantToken(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.ParmanentToken)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    
    func GetTempToken(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.TempToken)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
}

