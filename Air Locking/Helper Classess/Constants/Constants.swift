//
//  Constants.swift
//  Air Locking
//
//  Created by Digital Dividend on 5/21/18.
//  Copyright © 2018 Air Locking. All rights reserved.
//

import Foundation
import UIKit
struct Constants {
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
    
    static let USER_DEFAULTS               = UserDefaults.standard
    
    static let BaseURL                     = "https://airlocking.co/"
    static let WEBBASEURL                  = "https://airlocking.co/token-login/?temptoken="
    
//    static let SINGLETON                   = Singleton.sharedInstance
    
    // WEB SERVICES
    
    static let ParmanentToken = "permanent-token/"
    static let TempToken = "temporary-token/"
    static let ForgotPasswordWebURL = "https://airlocking.co/my-account/lost-password/"
    static let MobileRegistrationURL = "https://airlocking.co/mobileregister"
}

struct UserDefaultsContansts {
    static let PermenantToken = "PermenantToken"
    static let PinCode = "PinCode"
}
